#!/usr/bin/env bash

main () {
    other_name="$1"
    [[ "$#" -lt 1 ]] && other_name="you"

    printf "One for %s, one for me.\n" "$other_name"
}

# call main with all of the positional arguments
main "$@"
